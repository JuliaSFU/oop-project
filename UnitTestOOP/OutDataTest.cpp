#include "stdafx.h"
#include "CppUnitTest.h"
#include "Program.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace shapes;

namespace OutDataTest
{
	TEST_CLASS(OutDataTest)
	{
	public:

		TEST_METHOD(OutEmptyCont)
		{
			ofstream ofst("EmptyOutTest.txt");
			ofstream ifst("EmptyInTest.txt");
			container expected;
			expected.Out(ofst);

			ifstream ofs("EmptyOutTest.txt");
			ifstream ifs("EmptyInTest.txt");
			string s;
			string exp;
			while (!ifs.eof())
			{
				getline(ifs, exp);
				getline(ofs, s);
				Assert::AreEqual(exp, s);
			}
		}

		TEST_METHOD(OutFullCont)
		{
			ofstream ofst("OutTest.txt");
			ofstream ifst("InTest.txt");

			container expected;
			shape *tr1 = new triangle(0, 0, 1, 1, 2, 2, shape::color::YELLOW, 6.0);
			shape *c1 = new circle(0, 0, 1, shape::color::ORANGE, 5.1);
			shape *r1 = new rectangle(0, 0, 2, 2, shape::color::RED, 1.3);
			expected.InCont(tr1);
			expected.InCont(c1);
			expected.InCont(r1);
			expected.Out(ofst);

			ifst << "Container contains elements:" << endl;
			ifst << "It is Triangle: x1 = 0, y1 = 0, x2 = 1, y2 = 1, x3 = 2, y3 = 2, color: YELLOW" << endl;
			ifst << "density = 6" << endl;
			ifst << "perimeter = 5.65685" << endl;
			ifst << "It is Circle: a = 0, b = 0, r = 1, color: ORANGE" << endl;
			ifst << "density = 5.1" << endl;
			ifst << "perimeter = 6.28319" << endl;
			ifst <<"It is Rectangle: x1 = 0, y1 = 0, x2 = 2, y2 = 2, color: RED"<< endl;
			ifst << "density = 1.3" << endl;
			ifst << "perimeter = 8" << endl;

			ifstream ofs("OutTest.txt");
			ifstream ifs("InTest.txt");
			string s;
			string exp;
			while (!ifs.eof())
			{
				getline(ifs, exp);
				getline(ofs, s);
				Assert::AreEqual(exp, s);
			}
			
		}

		TEST_METHOD(OutRectTest)
		{
			ofstream ofst("OutRect.txt");
			ofstream ifst("InRect.txt");

			container expected;
			shape *tr1 = new triangle(0, 0, 1, 1, 2, 2, shape::color::YELLOW, 6.0);
			shape *c1 = new circle(0, 0, 1, shape::color::ORANGE, 5.1);
			shape *r1 = new rectangle(0, 0, 2, 2, shape::color::RED, 1.3);
			expected.InCont(tr1);
			expected.InCont(c1);
			expected.InCont(r1);
			expected.OutRect(ofst);

			ifst << "Only rectangles." << endl;
			ifst << "It is Rectangle: x1 = 0, y1 = 0, x2 = 2, y2 = 2, color: RED" << endl;
			ifst << "density = 1.3" << endl;
			ifst << "perimeter = 8" << endl;

			ifstream ofs("OutRect.txt");
			ifstream ifs("InRect.txt");
			string s;
			string exp;
			while (!ifs.eof())
			{
				getline(ifs, exp);
				getline(ofs, s);
				Assert::AreEqual(exp, s);
			}

		}
	};
}
