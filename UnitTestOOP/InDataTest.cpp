#include "stdafx.h"
#include "CppUnitTest.h"
#include "Program.h"
#include <iostream>
#include <fstream>

using namespace std;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace shapes;

namespace InDataTest
{
	TEST_CLASS(InDataTest)
	{
	public:

		TEST_METHOD(InEmptyFile)
		{
			ifstream ifst("EmptyFile.txt");
			container c;
			c.In(ifst);
			container expected;
			Assert::AreEqual(expected.head==NULL, c.head==NULL);
		}

		TEST_METHOD(InFullFile)
		{
			ifstream ifst("FullFile.txt");
			container c;
			c.In(ifst);

			container expected;
			shape *r1 = new rectangle(0, 0, 2, 2, shape::color::RED, 1.3);
			shape *c1 = new circle(0, 0, 1, shape::color::ORANGE, 5.1);
			shape *c2 = new circle(1, 2, 2, shape::color::PURPLE, 7.5);
			shape *tr1 = new triangle(0, 0, 1, 1, 2, 2, shape::color::YELLOW, 6.0);
			shape *tr2 = new triangle(1, 2, 3, 4, 5, 6, shape::color::ORANGE, 2.1);
			expected.InCont(r1);
			expected.InCont(c1);
			expected.InCont(c2);
			expected.InCont(tr1);
			expected.InCont(tr2);

			for (int i = 0; i < 5; i++)
			{
				shape* s = c.OutCont(i);
				shape* expect = expected.OutCont(i);
				Assert::AreEqual(expect->StringForAssert(), s->StringForAssert());
			}
		}

		TEST_METHOD(InNonexistentFile)
			 {
			ifstream ifst("C://in.txt");
			container c;
			c.In(ifst);
			container expected;
			Assert::AreEqual(expected.head == NULL, c.head == NULL);
			}
	};
}
